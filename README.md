# READER FOR GOOGLE NEWS #

[Download .apk](https://bitbucket.org/dristov/google-news-reader/raw/f6409802863b15dc27111417f4337789e4800781/apk/newsreader.apk)

## Description

This project is an Android application which reads the news from Google News RSS and displays them in a list. The user is able to have favorites and labels and also search for desired news. It uses the popular open source library Retrofit and SimpleXML to fetch the contents and parse them to Java objects.

- Written in Kotlin – an alternative JVM language from JetBrains
- Parsing XML to Java objects
- Using Realm - a mobile NoSQL database
- Providing a Light/Dark theme
- Using Dagger library for dependency injection
- Building user interfaces in XML
- Using Chrome Browser to display in-app links
- Implementing the new bottom navigation design
- Using third-party open source libraries

## Screenshots

![News](https://bytebucket.org/dristov/google-news-reader/raw/3e966c891f1aef6b9066e2cccaa22eb5ab01c118/screenshots/news.png)