package com.dristov.rssapp.data

import com.dristov.rssapp.data.rss.RssService
import com.dristov.rssapp.models.Rss
import com.dristov.rssapp.util.TopicManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FeedRepositoryImpl @Inject constructor(val rssService: RssService) : FeedRepository {

    private var feedCall: Call<Rss>? = null

    override fun loadFeed(topic: String, callback: LoadFeedCallback) {

        if (!topic.equals(TopicManager.TOPIC_TOP_STORIES)) {
            feedCall = rssService.getFeedForTopic(topic)
        } else {
            feedCall = rssService.topStories
        }

        feedCall?.enqueue(feedResponse(callback))
    }

    override fun searchFeed(query: String, callback: LoadFeedCallback) {
        feedCall = rssService.getFeedsForSearch(query)
        feedCall?.enqueue(feedResponse(callback))
    }

    fun feedResponse(cb: LoadFeedCallback) : Callback<Rss> {

        return object : Callback<Rss> {

            override fun onFailure(call: Call<Rss>?, t: Throwable?) {
                cb.onFeedError()
            }

            override fun onResponse(call: Call<Rss>?, response: Response<Rss>?) {

                if (response != null && response.isSuccessful) {
                    cb.onFeedLoaded(response.body().channel.items)
                } else {
                    cb.onFeedError()
                }
            }
        }
    }

    override fun cancelCall() {

        if (feedCall != null) {

            if (feedCall!!.isCanceled) {
                feedCall!!.cancel()
            }
        }
    }
}