package com.dristov.rssapp.data

import com.dristov.rssapp.models.FeedItem

interface LoadFeedCallback {

    fun onFeedLoaded(feedItems: List<FeedItem>?)
    fun onFeedError()
}