package com.dristov.rssapp.data.rss;

import com.dristov.rssapp.models.Rss;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RssService {

    @GET("news/section?cf=all&pz=1&ned=us&output=rss")
    Call<Rss> getFeedForTopic(@Query("topic") String topic);

    @GET("news?cf=all&ned=us&output=rss")
    Call<Rss> getTopStories();

    @GET("news?cf=all&pz=1&ned=us&output=rss")
    Call<Rss> getFeedsForSearch(@Query("q") String searchQuery);
}