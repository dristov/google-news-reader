package com.dristov.rssapp.data

import com.dristov.rssapp.models.Bookmark
import com.dristov.rssapp.models.FeedHistoryItem
import com.dristov.rssapp.models.Label
import com.dristov.rssapp.models.SearchItem
import io.realm.Realm
import io.realm.RealmObject
import io.realm.Sort
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class DbService @Inject constructor(val realm: Realm) {


    fun addBookmark(title: String, url: String) {

        realm.executeTransaction {

            val bookmark = it.createObject(Bookmark::class.java)
            bookmark.title = title
            bookmark.url = url
            bookmark.created = System.currentTimeMillis()
        }
    }

    fun loadBookmarks(): List<Bookmark> {
        return realm.where(Bookmark::class.java).findAllSorted("created", Sort.DESCENDING)
    }

    fun deleteBookmark(url: String) {

        realm.executeTransaction {

            it.where(Bookmark::class.java).equalTo("url", url).findFirst()?.removeFromRealm()
        }
    }

    fun saveSearchItem(query: String) {

        //val isNull = realm.where(SearchItem::class.java).equalTo("query", query).findFirst() == null
        val itemExists = exists("query", query, SearchItem::class.java)

        if (!itemExists) {

            realm.executeTransaction {
                val item = it.createObject(SearchItem::class.java)
                item.query = query
            }
        }
    }

    fun loadSearchItems(): List<SearchItem> {

        return realm.where(SearchItem::class.java).findAll()
    }

    fun clearSearchHistory() {
        realm.executeTransaction { it.clear(SearchItem::class.java) }
    }

    fun loadLabels(): List<Label> {
        return realm.where(Label::class.java).findAll()
    }

    fun findLabel(id: Long): Label {
        return realm.where(Label::class.java).equalTo("id", id).findFirst()
    }

    fun saveLabel(text: String): Long {

        val id = System.currentTimeMillis()

        realm.executeTransaction {
            val label = it.createObject(Label::class.java)
            label.id = id
            label.text = text
        }

        return id
    }

    fun deleteLabel(id: Long) {

        realm.executeTransaction {
            it.where(Label::class.java).equalTo("id", id).findFirst()?.removeFromRealm()
        }
    }

    fun addHistoryItem(url: String, title: String) {

        if (exists("url", url, FeedHistoryItem::class.java)) {
            return
        }

        realm.executeTransaction {

            val item = it.createObject(FeedHistoryItem::class.java)
            item.url = url
            item.title = title
            item.date = Date()
        }
    }

    fun loadHistoryItems(): List<FeedHistoryItem> {
        return realm.where(FeedHistoryItem::class.java).findAllSorted("date", Sort.DESCENDING)
    }

    fun clearHistory() {
        realm.executeTransaction { it.clear(FeedHistoryItem::class.java) }
    }

    fun <T : RealmObject> exists(key: String, value: String, clazz: Class<T>): Boolean {
        return realm.where(clazz).equalTo(key, value).findFirst() != null
    }
}