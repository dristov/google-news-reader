package com.dristov.rssapp.data


interface FeedRepository {

    fun loadFeed(topic: String, callback: LoadFeedCallback)
    fun searchFeed(query: String, callback: LoadFeedCallback)
    fun cancelCall()
}