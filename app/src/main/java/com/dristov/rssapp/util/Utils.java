package com.dristov.rssapp.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Uri;

public final class Utils {

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo() != null;
    }

    public static String getDomainName(String url) {

        Uri parse = Uri.parse(url);
        String host = parse.getHost();

        if (host.startsWith("www")) {
            host = host.substring(4);
        }

        return host;
    }
}