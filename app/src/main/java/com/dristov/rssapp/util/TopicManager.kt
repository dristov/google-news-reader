package com.dristov.rssapp.util

import com.dristov.rssapp.models.Topic

object TopicManager {

    val TOPIC_TOP_STORIES = "top_stories"

    val topicList = arrayOf(

            Topic(TOPIC_TOP_STORIES, "Top Stories"),
            Topic("w", "World"),
            Topic("b", "Business"),
            Topic("e", "Entertainment"),
            Topic("s", "Sports"),
            Topic("tc", "Technology"),
            Topic("m", "Health"),
            Topic("snc", "Science")
    )
}