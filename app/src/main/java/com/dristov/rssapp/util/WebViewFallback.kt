package com.dristov.rssapp.util

import android.app.Activity
import android.content.Intent
import android.net.Uri
import com.dristov.rssapp.R
import com.dristov.rssapp.util.customtabs.CustomTabActivityHelper
import com.dristov.rssapp.ui.feed.WebViewActivity


class WebViewFallback : CustomTabActivityHelper.CustomTabFallback {

    override fun openUri(activity: Activity?, uri: Uri?) {

        val intent = Intent(activity, WebViewActivity::class.java)
        intent.putExtra(WebViewActivity.EXTRA_WEB_URL, uri.toString())
        activity!!.startActivity(intent)
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
    }
}