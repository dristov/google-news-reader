package com.dristov.rssapp.extensions

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.preference.PreferenceManager
import android.support.customtabs.CustomTabsIntent
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.dristov.rssapp.R
import com.dristov.rssapp.util.WebViewFallback
import com.dristov.rssapp.util.customtabs.CustomTabActivityHelper


inline fun <reified T : View> Activity.bind(id: Int): T = findViewById(id) as T

inline fun <reified T : Any> IntentFor(context: Context): Intent = Intent(context, T::class.java)

fun Activity.openChromeCustomTab(url: String) {

    val intentBuilder = CustomTabsIntent.Builder()
    intentBuilder.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary))
    intentBuilder.setShowTitle(true)
    intentBuilder.setStartAnimations(this, R.anim.slide_in_right, R.anim.slide_out_left)
    intentBuilder.setExitAnimations(this, R.anim.slide_in_left, R.anim.slide_out_right)

    val intent = intentBuilder.build()
    CustomTabActivityHelper.openCustomTab(this, intent, Uri.parse(url), WebViewFallback())
}

fun Fragment.defaultSharedPreferences() = PreferenceManager.getDefaultSharedPreferences(activity)

inline fun consume(f: () -> Unit): Boolean {
    f()
    return true
}

fun AppCompatActivity.showFragment(frag: Fragment, tag: String, layoutRes: Int = R.id.content_frame, toStack: Boolean = true) {

    val transaction = supportFragmentManager
            .beginTransaction()
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
            .replace(layoutRes, frag, tag)

    if (toStack) {
        transaction.addToBackStack(null)
    }

    transaction.commit()
}

inline fun <reified T : Any> Activity.transitionToActivity() {
    val optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this)
    startActivity(IntentFor<T>(this), optionsCompat.toBundle())
}

fun Activity.transitionToActivity(intent: Intent) {
    val optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this)
    startActivity(intent, optionsCompat.toBundle())
}


fun log_i(str: String) {
    Log.i("__LOG", str)
}

fun isLollipop(): Boolean {
    return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
}