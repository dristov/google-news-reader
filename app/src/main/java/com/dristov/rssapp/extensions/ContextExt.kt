package com.dristov.rssapp.extensions

import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.preference.PreferenceManager
import android.support.v4.app.ActivityOptionsCompat
import android.support.v7.app.AppCompatActivity


val Context.defaultSharedPreferences: SharedPreferences
    get() = PreferenceManager.getDefaultSharedPreferences(this)

fun Context.isNetworkAvailable(): Boolean {

    val manager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val netInfo = manager.activeNetworkInfo
    return netInfo != null && netInfo.isConnected
}