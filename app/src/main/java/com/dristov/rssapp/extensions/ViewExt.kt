package com.dristov.rssapp.extensions

import android.app.Activity
import android.support.annotation.LayoutRes
import android.support.design.widget.Snackbar
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.dristov.rssapp.ui.recyclerview.ItemTouchHelperAdapter
import com.dristov.rssapp.util.RoundedCornersTransformation
import com.dristov.rssapp.ui.recyclerview.SimpleItemTouchHelperCallback
import com.squareup.picasso.Picasso


fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

fun ImageView.loadWithPicasso(url: String) {
    Picasso.with(context).load(url).fit().transform(RoundedCornersTransformation(8, 0)).into(this)
}

fun Activity.snackbar(parent: View, message: String, length: Int = Snackbar.LENGTH_LONG) {
    val snack = Snackbar.make(parent, message, length)
    snack.show()
}

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.hide() {
    this.visibility = View.GONE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun RecyclerView.addSwipeToDismiss(adapter: ItemTouchHelperAdapter) {

    val callback = SimpleItemTouchHelperCallback(adapter, false, true)
    val touchHelper = ItemTouchHelper(callback)
    touchHelper.attachToRecyclerView(this)
}

inline fun <reified T : View> View.bind(id: Int): T = this.findViewById(id) as T