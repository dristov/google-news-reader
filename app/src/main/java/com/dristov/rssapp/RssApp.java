package com.dristov.rssapp;

import android.app.Application;
import android.content.Context;

import com.dristov.rssapp.di.AppComponent;
import com.dristov.rssapp.di.AppModule;
import com.dristov.rssapp.di.DaggerAppComponent;
import com.dristov.rssapp.di.NetModule;

public class RssApp extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(this.getApplicationContext()))
                .build();
    }

    public static AppComponent getComponent(Context context) {
        return ((RssApp)context.getApplicationContext()).appComponent;
    }
}