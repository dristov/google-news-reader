package com.dristov.rssapp.models

data class Topic (val shortName: String, val title: String)