package com.dristov.rssapp.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.format.DateUtils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Root(name = "item", strict = false)
public final class FeedItem implements Parcelable {

    @Element(name = "pubDate")
    private String pubDate;

    @Element(name = "title")
    private String title;

    @Element(name = "category", required = false)
    private String category;

    @Element(name = "description")
    private String description;

    @Element(name = "link")
    private String link;

    public FeedItem() {}

    protected FeedItem(Parcel in) {
        pubDate = in.readString();
        title = in.readString();
        category = in.readString();
        description = in.readString();
        link = in.readString();
    }

    public static final Creator<FeedItem> CREATOR = new Creator<FeedItem>() {
        @Override
        public FeedItem createFromParcel(Parcel in) {
            return new FeedItem(in);
        }

        @Override
        public FeedItem[] newArray(int size) {
            return new FeedItem[size];
        }
    };

    public String getPubDate ()
    {
        return pubDate;
    }

    public void setPubDate (String pubDate)
    {
        this.pubDate = pubDate;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getCategory ()
    {
        return category;
    }

    public void setCategory (String category)
    {
        this.category = category;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getLink ()
    {
        return link;
    }

    public void setLink (String link)
    {
        this.link = link;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(pubDate);
        dest.writeString(title);
        dest.writeString(category);
        dest.writeString(description);
        dest.writeString(link);
    }


    private Date stringToDate() {

        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy HH:mm:ss", Locale.getDefault());

        try {
            return format.parse(this.pubDate.substring(5, this.pubDate.lastIndexOf(" ")));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public CharSequence getRelativeDate() {

        Date date = this.stringToDate();

        if (date != null) {

            return DateUtils.getRelativeTimeSpanString(
                    date.getTime(),
                    System.currentTimeMillis(),
                    0L,
                    DateUtils.FORMAT_ABBREV_ALL);
        }

        return "";
    }

    public String getImageUrl() {

        Document doc = Jsoup.parse(this.description);
        org.jsoup.nodes.Element img = doc.select("img").first();

        if (img != null) {
            return "http:" + img.attr("src");
        }

        return null;
    }

    public String getPublisherName() {
        return title.substring(title.lastIndexOf("-") + 2);
    }

    public String getExactUrl() {
        return this.link.substring(this.link.lastIndexOf("http"));
    }
}