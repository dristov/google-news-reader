package com.dristov.rssapp.models

import io.realm.RealmObject
import io.realm.annotations.Index
import java.util.*

open class FeedHistoryItem : RealmObject() {

    open var title: String? = null

    @Index
    open var url: String? = null

    open var date: Date? = null

}