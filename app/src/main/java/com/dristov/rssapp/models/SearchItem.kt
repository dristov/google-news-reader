package com.dristov.rssapp.models

import io.realm.RealmObject
import io.realm.annotations.Index


open class SearchItem : RealmObject() {

    @Index
    open var query: String? = null
}