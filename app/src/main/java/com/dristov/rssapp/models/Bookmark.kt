package com.dristov.rssapp.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey


open class Bookmark : RealmObject() {

    open var title: String? = null
    open var url: String? = null

    @PrimaryKey
    open var created: Long? = null
}