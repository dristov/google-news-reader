package com.dristov.rssapp.models

import io.realm.RealmObject
import io.realm.annotations.Index
import io.realm.annotations.PrimaryKey


open class Label : RealmObject() {

    @PrimaryKey
    open var id: Long? = null

    @Index
    open var text: String? = null
}