package com.dristov.rssapp.di;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.preference.PreferenceManager;

import com.dristov.rssapp.RssApp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;

@Module
public class AppModule {

    RssApp app;

    public AppModule(RssApp app) {
        this.app = app;
    }

    @Provides
    @Singleton
    public RssApp provideApp() {
        return app;
    }

    @Provides
    @Singleton
    public Context provideAppContext() {
        return this.app.getApplicationContext();
    }

    @Provides
    @Singleton
    public SharedPreferences provideSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(app);
    }

    @Provides
    @Singleton
    public Realm provideRealm() {
        return Realm.getInstance(app);
    }
}