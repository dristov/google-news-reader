package com.dristov.rssapp.di;

import android.content.Context;
import android.util.Log;

import com.dristov.rssapp.RssApp;
import com.dristov.rssapp.data.FeedRepository;
import com.dristov.rssapp.data.FeedRepositoryImpl;
import com.dristov.rssapp.data.rss.RssService;
import com.dristov.rssapp.util.Utils;

import java.io.File;
import java.io.IOException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

@Module
public class NetModule {

    private static final String BASE_URL = "http://news.google.com/";
    private Context context;

    public NetModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    Cache provideCache(RssApp app) {

        File cacheDir = new File(app.getCacheDir(), "cache");
        long cacheSize = 10 * 1024 * 1024; // 10 mb
        return new Cache(cacheDir, cacheSize);
    }

    @Provides
    @Singleton
    public Interceptor provideCacheInterceptor() {

        return new Interceptor() {

            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {

                Log.d("DataModule", "intercept: called");
                Request request = chain.request();

                if (Utils.isNetworkAvailable(context)) {

                    Log.d("NetModule", "intercept: network is available");

                    // show from cache for 1 hour
                    request = request.newBuilder()
                            .header("Cache-Control", "max-age=" + 3600)
                            .build();
                } else {

                    Log.d("NetModule", "intercept: network not available");

                    // show from up to 7 days ago
                    request = request.newBuilder()
                            .header("Cache-Control", "only-if-cached, max-stale=" + 60 * 60 * 24 * 7)
                            .build();
                }

                return chain.proceed(request);
            }
        };
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(Cache cache, Interceptor interceptor) {

        return new OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor(interceptor)
                .build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient client) {

        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    RssService provideRssService(Retrofit retrofit) {
        return retrofit.create(RssService.class);
    }

    @Provides
    @Singleton
    FeedRepository provideFeedRepository(RssService service) {
        return new FeedRepositoryImpl(service);
    }
}