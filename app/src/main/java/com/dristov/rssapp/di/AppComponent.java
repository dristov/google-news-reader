package com.dristov.rssapp.di;

import com.dristov.rssapp.ui.MainActivity;
import com.dristov.rssapp.ui.base.BaseActivity;
import com.dristov.rssapp.ui.bookmarks.BookmarksFragment;
import com.dristov.rssapp.ui.feed.FeedListFragment;
import com.dristov.rssapp.ui.feed.NewsFragment;
import com.dristov.rssapp.ui.history.HistoryFragment;
import com.dristov.rssapp.ui.labels.LabelsFragment;
import com.dristov.rssapp.ui.search.SearchActivity;
import com.dristov.rssapp.ui.settings.SettingsFragment;

import javax.inject.Singleton;

import dagger.Component;

@Component(
        modules = {
                AppModule.class,
                NetModule.class
        }
)
@Singleton
public interface AppComponent {

    void inject(BaseActivity baseActivity);
    void inject(MainActivity activity);

    void inject(NewsFragment fragment);
    void inject(SettingsFragment fragment);

    // presenters
    void inject(FeedListFragment fragment);
    void inject(BookmarksFragment fragment);
    void inject(HistoryFragment fragment);
    void inject(SearchActivity activity);
    void inject(LabelsFragment fragment);
}