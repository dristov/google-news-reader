package com.dristov.rssapp.ui.settings

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.webkit.WebView
import com.dristov.rssapp.R
import com.dristov.rssapp.extensions.bind

class OpenSourceActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_open_source)

        val toolbar = bind<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val webView = bind<WebView>(R.id.web_view)
        webView.loadUrl("file:///android_res/raw/open_source_licences.html")
    }
}