package com.dristov.rssapp.ui.feed

import com.dristov.rssapp.data.DbService
import com.dristov.rssapp.data.FeedRepository
import com.dristov.rssapp.data.LoadFeedCallback
import com.dristov.rssapp.extensions.log_i
import com.dristov.rssapp.models.FeedItem
import com.dristov.rssapp.ui.base.BasePresenter
import io.realm.RealmObject
import javax.inject.Inject

class FeedPresenter @Inject constructor(
        val repo: FeedRepository,
        val dbService: DbService)

    : BasePresenter<FeedView>(), LoadFeedCallback {

    override fun detachView() {
        super.detachView()
        repo.cancelCall()
    }

    fun loadFeeds(topic: String, fromRefresh: Boolean = false) {

        if (!fromRefresh) {
            mvpView.showLoadingBar()
        }

        log_i("loading feeds $topic")
        repo.loadFeed(topic, this)
    }

    fun searchFeeds(query: String) {
        mvpView.showLoadingBar()
        repo.searchFeed(query, this)
    }

    override fun onFeedLoaded(feedItems: List<FeedItem>?) {

        if (isViewAttached) {

            mvpView.hideLoadingBar()

            if (feedItems != null && feedItems.size > 0) {
                mvpView.setItems(feedItems)
            } else {
                mvpView.showErrorScreen()
            }
        }
    }

    override fun onFeedError() {

        if (isViewAttached) {
            mvpView.hideLoadingBar()
            mvpView.showErrorScreen()
        }
    }

    fun showSavePageDialog(item: FeedItem) {
        mvpView.showSaveDialog(item)
    }

    fun addToBookmarks(title: String, url: String) {
        dbService.addBookmark(title, url)
    }

    fun removeFromBookmarks(url: String) {
        dbService.deleteBookmark(url)
    }

    fun addItemToHistory(url: String, title: String) {
        dbService.addHistoryItem(url, title)
    }

    fun openItemInBrowser(link: String) {
        mvpView.openInBrowser(link)
    }

    fun shareLink(url: String) {
        mvpView.showShareDialog(url)
    }

    fun <T: RealmObject> itemExists(url: String, clazz: Class<T>): Boolean {
        return dbService.exists("url", url, clazz)
    }
}