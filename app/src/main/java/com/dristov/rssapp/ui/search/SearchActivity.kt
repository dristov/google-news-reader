package com.dristov.rssapp.ui.search

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.transition.TransitionInflater
import android.view.KeyEvent
import android.view.Menu
import android.view.MenuItem
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.ImageView
import com.dristov.rssapp.R
import com.dristov.rssapp.RssApp
import com.dristov.rssapp.extensions.*
import com.dristov.rssapp.models.SearchItem
import com.dristov.rssapp.ui.base.BaseActivity
import com.dristov.rssapp.ui.feed.FeedListFragment
import javax.inject.Inject

class SearchActivity : BaseActivity(), SearchNewsView, TextWatcher {

    interface SearchQueryListener {
        fun onQuerySubmit(text: String)
    }

    override val layoutResource: Int
        get() = R.layout.activity_search

    companion object {

        val TAG_FRAGMENT_FEED = "feedFragment"

        val EXTRA_SEARCH_QUERY = "extraSearchQuery"
    }

    @Inject
    lateinit var presenter: SearchPresenter

    private val inputQuery by lazy { bind<AutoCompleteTextView>(R.id.inputQuery) }
    private val toolbar by lazy { bind<Toolbar>(R.id.toolbarSearch) }
    private val clearTextBtn by lazy { bind<ImageView>(R.id.clear_text) }
    private var listener: SearchQueryListener? = null

    private lateinit var feedFragment: FeedListFragment
    private lateinit var adapter: ArrayAdapter<String>

    override fun onCreateCont(savedInstanceState: Bundle?) {

        RssApp.getComponent(this).inject(this)

        presenter.attachView(this)

        setupTransition()

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setupFeedFragment()
        setupSearchKeyListener()

        presenter.loadRecentSearches()
    }

    fun setupFeedFragment() {

        feedFragment = FeedListFragment()

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.content_frame, feedFragment, TAG_FRAGMENT_FEED)
                .commit()

        listener = feedFragment
    }

    fun checkIntentExtras() {

        if (intent.hasExtra(EXTRA_SEARCH_QUERY)) {
            val searchQuery = intent.extras.getString(EXTRA_SEARCH_QUERY, "")
            showFeedForSearch(searchQuery)

        } else {
            inputQuery.requestFocus()
        }
    }

    override fun setRecentSearches(items: List<SearchItem>) {

        val searches = items.map { it.query }.toList()

        adapter = ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, searches)

        inputQuery.threshold = 0
        inputQuery.setAdapter(adapter)
    }

    override fun onSearchHistoryCleared() {
        adapter.clear()
    }

    private fun setupSearchKeyListener() {

        clearTextBtn.hide()
        clearTextBtn.setOnClickListener {
            inputQuery.setText("")
            inputQuery.requestFocus()
        }

        inputQuery.setOnKeyListener { view, keyCode, event ->

            if (event.action == KeyEvent.ACTION_DOWN) {

                if (keyCode == KeyEvent.KEYCODE_ENTER) {

                    hideKeyboard()
                    inputQuery.clearFocus()

                    showFeedForSearch(inputQuery.text.toString())

                    return@setOnKeyListener true
                }
            }

            return@setOnKeyListener false
        }

        inputQuery.addTextChangedListener(this)
    }

    fun showFeedForSearch(query: String) {

        presenter.saveSearchQuery(query)

        if (inputQuery.text.isEmpty()) {
            inputQuery.setText(query)
        }

        listener?.onQuerySubmit(query)
    }

    override fun afterTextChanged(s: Editable?) {

        if (s?.length == 0) {
            clearTextBtn.hide()
        } else {
            clearTextBtn.show()
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

    private fun hideKeyboard() {
        val imm = applicationContext
                .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(inputQuery.windowToken, 0)
    }

    private fun setupTransition() {

        if (isLollipop()) {

            val transit = TransitionInflater.from(this).inflateTransition(R.transition.search_enter)
            window.enterTransition = transit

            val out = TransitionInflater.from(this).inflateTransition(R.transition.search_exit)
            window.returnTransition = out
        }
    }

    override fun onDetachedFromWindow() {
        listener = null
        super.onDetachedFromWindow()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?) = when(item?.itemId) {

        R.id.action_clear_search -> consume { presenter.clearSearches() }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }
}