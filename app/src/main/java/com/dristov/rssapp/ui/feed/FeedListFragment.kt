package com.dristov.rssapp.ui.feed

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dristov.rssapp.R
import com.dristov.rssapp.RssApp
import com.dristov.rssapp.extensions.*
import com.dristov.rssapp.models.Bookmark
import com.dristov.rssapp.models.FeedItem
import com.dristov.rssapp.ui.base.BaseLazyFragment
import com.dristov.rssapp.ui.search.SearchActivity
import com.dristov.rssapp.ui.widget.SaveBookmarkDialog
import com.dristov.rssapp.util.MenuSheetView
import com.dristov.rssapp.util.PocketUtils
import com.dristov.rssapp.util.SimpleDividerItemDecoration
import kotlinx.android.synthetic.main.list_feed.*
import java.net.URLEncoder
import java.util.*
import javax.inject.Inject


class FeedListFragment : BaseLazyFragment(), SearchActivity.SearchQueryListener, FeedView {

    private val rssItems = ArrayList<FeedItem>()
    private val feedAdapter by lazy {
        FeedAdapter(rssItems, dlImg)
    }

    var isViewPrepared = false
    var feedLoaded = false

    @Inject
    lateinit var feedPresenter: FeedPresenter

    private val dlImg: Boolean
        get() = defaultSharedPreferences().getBoolean("feed_download_images", true)

    private var argTopic: String? = null

    companion object {

        private val ARG_TOPIC = "argTopic"
        private val KEY_RSS_ITEMS = "keyRssItems"
        private val KEY_LOADED = "keyFeedLoaded"

        fun newInstance(topic: String): FeedListFragment {

            val args = Bundle()
            args.putString(ARG_TOPIC, topic)

            val fragment = FeedListFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        RssApp.getComponent(activity).inject(this)
        feedPresenter.attachView(this)

        if (savedInstanceState != null) {

            argTopic = savedInstanceState.getString(ARG_TOPIC, null)
            feedLoaded = savedInstanceState.getBoolean(KEY_LOADED)

            val items = savedInstanceState.getParcelableArrayList<FeedItem>(KEY_RSS_ITEMS)

            if (items != null) {
                log_i("restoring feed items for $argTopic from saved state")
                feedAdapter.clearItems()
                feedAdapter.setItems(items)
            }

        } else {
            setupFragmentArgs()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container!!.inflate(R.layout.list_feed)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        isViewPrepared = true

        hideLoadingBar()
        setupRecyclerView()
        loadContent()

        if (activity is SearchActivity) {
            disableSwipeToRefresh()
        } else {
            setupSwipeToRefresh()
        }
    }

    override fun onStart() {
        super.onStart()

        if (activity is SearchActivity) {
            (activity as SearchActivity).checkIntentExtras()
        }
    }

    override fun loadContent() {

        if(!isViewPrepared || !isVisible) {
            return;
        }

        if (argTopic != null && feedAdapter.itemCount == 0 && !feedLoaded) {

            feedPresenter.loadFeeds(argTopic!!)
        }
    }

    private fun setupFragmentArgs() {

        if (this.arguments != null) {
            argTopic = this.arguments.getString(ARG_TOPIC, null)
        }
    }

    private fun setupRecyclerView() {

        log_i("setupRecyclerView")

        feedList.setLayoutSpan(activity)
        feedList.setHasFixedSize(true)
        feedList.addItemDecoration(SimpleDividerItemDecoration(activity))

        feedAdapter.onItemClick = { position -> onItemClick(position) }
        feedAdapter.onMenuClick = { view, pos -> showBottomsheetMenu(pos) }
        feedList.adapter = feedAdapter

        feedList.alpha = 0.0f
    }

    private fun setupSwipeToRefresh() {

        swipeRefresh.setOnRefreshListener {

            if (activity.isNetworkAvailable()) {
                feedPresenter.loadFeeds(argTopic!!, true)
            }
        }
    }

    fun disableSwipeToRefresh() {
        swipeRefresh.isEnabled = false
    }

    override fun setItems(feedItems: List<FeedItem>) {

        feedLoaded = true

        if (swipeRefresh.isEnabled && swipeRefresh.isRefreshing) {
            swipeRefresh.isRefreshing = false
        }

        if (feedAdapter.itemCount > 0) {
            feedAdapter.clearItems()
        }

        feedAdapter.setItems(feedItems)

        feedList.animate()
            .alpha(1.0f)
            .setDuration(300)
            .start()
    }

    override fun showSnackbar(message: String) {
    }

    override fun showErrorScreen() {
        feedAdapter.setItems(ArrayList<FeedItem>())
    }

    override fun showLoadingBar() {
        loadingBar.show()
    }

    override fun hideLoadingBar() {
        loadingBar.hide()
    }

    override fun openInBrowser(url: String) {
        activity.openChromeCustomTab(url)
    }

    override fun showSaveDialog(item: FeedItem) {

        val dialog = SaveBookmarkDialog.newInstance(item.title)
        dialog.setListener { text -> feedPresenter.addToBookmarks(text, item.exactUrl) }
        dialog.show(activity.supportFragmentManager, "save_bookmark")
    }

    override fun showShareDialog(url: String) {

        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, url)
        activity.startActivity(Intent.createChooser(intent, "Share"))
    }

    override fun onQuerySubmit(text: String) {

        if (!TextUtils.isEmpty(text)) {

            if (feedAdapter.itemCount > 0) {
                feedAdapter.clearItems()
            }

            // encode the url for the query string
            val urlEncoded = URLEncoder.encode(text, "utf-8")
            feedPresenter.searchFeeds(urlEncoded)
        }
    }

    fun onItemClick(position: Int) {

        feedPresenter.addItemToHistory(rssItems[position].exactUrl, rssItems[position].title)
        feedPresenter.openItemInBrowser(rssItems[position].link)
    }

    private fun showBottomsheetMenu(pos: Int) {

        var isInBookmarks = false

        if (feedPresenter.itemExists(rssItems[pos].exactUrl, Bookmark::class.java)) {
            isInBookmarks = true
        }

        val menuSheetView = MenuSheetView(activity, rssItems[pos].title) {

            if (article_sheet_menu.isSheetShowing) {
                article_sheet_menu.dismissSheet()
            }

            when (it.itemId) {

                R.id.add_bookmark -> {

                    if (isInBookmarks) {
                        feedPresenter.removeFromBookmarks(rssItems[pos].exactUrl)

                    } else {
                        feedPresenter.showSavePageDialog(rssItems[pos])
                    }

                }

                R.id.share -> feedPresenter.shareLink(rssItems[pos].link)

                R.id.search_publisher -> searchForPublisher(rssItems[pos].publisherName)

                R.id.pocket -> saveToPocket(rssItems[pos].exactUrl)
            }

            return@MenuSheetView true
        }

        menuSheetView.inflateMenu(R.menu.sheet_article)

        val action = menuSheetView.menu.findItem(R.id.add_bookmark)
        action.title =  if (isInBookmarks) "Remove from bookmarks" else "Add to bookmarks"
        action.icon =  if (isInBookmarks) ContextCompat.getDrawable(activity, R.drawable.ic_delete_grey_600_24dp)
        else ContextCompat.getDrawable(activity, R.drawable.heart)

        menuSheetView.menu.findItem(R.id.search_publisher).title = "${rssItems[pos].publisherName}"

        article_sheet_menu.showWithSheetView(menuSheetView)
    }

    private fun searchForPublisher(publisher: String) {

        val intent = Intent(activity, SearchActivity::class.java)
        intent.putExtra(SearchActivity.EXTRA_SEARCH_QUERY, publisher)
        activity.transitionToActivity(intent)
    }

    private fun saveToPocket(url: String) {

        if (!PocketUtils.isPocketInstalled(activity)) {

            AlertDialog.Builder(activity, R.style.DefaultDialogStyle)
                .setTitle("Install Pocket")
                .setMessage("Please install Pocket to save this article")
                .setPositiveButton("OK", null)
                .setCancelable(true)
                .show()

        } else {
            PocketUtils.addToPocket(activity, url)
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        if (feedLoaded) {

            outState!!.putString(ARG_TOPIC, argTopic)
            outState.putParcelableArrayList(KEY_RSS_ITEMS, rssItems)
            outState.putBoolean(KEY_LOADED, feedLoaded)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        feedPresenter.detachView()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        isViewPrepared = false
    }
}