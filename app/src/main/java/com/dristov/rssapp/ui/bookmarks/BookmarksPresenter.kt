package com.dristov.rssapp.ui.bookmarks

import com.dristov.rssapp.data.DbService
import com.dristov.rssapp.ui.base.BasePresenter
import javax.inject.Inject

class BookmarksPresenter @Inject constructor (val dbService: DbService) : BasePresenter<BookmarksView>() {

    fun loadBookmarks() {
        mvpView.setBookmarks(dbService.loadBookmarks())
    }

    fun deleteBookmark(url: String) {
        dbService.deleteBookmark(url)
    }
}