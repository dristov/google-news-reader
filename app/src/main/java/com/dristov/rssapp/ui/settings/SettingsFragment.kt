package com.dristov.rssapp.ui.settings

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.preference.PreferenceFragmentCompat
import android.support.v7.preference.PreferenceManager
import android.view.Menu
import com.dristov.rssapp.R
import com.dristov.rssapp.RssApp
import com.dristov.rssapp.ui.MainActivity


class SettingsFragment : PreferenceFragmentCompat() {

    private lateinit var prefListener: SharedPreferences.OnSharedPreferenceChangeListener

    override fun onCreatePreferences(p0: Bundle?, p1: String?) {
        addPreferencesFromResource(R.xml.settings)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        RssApp.getComponent(activity).inject(this)
        setHasOptionsMenu(true)
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        menu!!.clear()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        prefListener = SharedPreferences.OnSharedPreferenceChangeListener { sharedPreferences, key ->

            if (key.equals("theme_dark")) {

                AlertDialog.Builder(activity, R.style.DefaultDialogStyle)
                        .setTitle("Restart Application")
                        .setMessage("You need to restart the application to change the theme")
                        .setPositiveButton("RESTART", { dialogInterface, i ->  restartApp() })
                        .setNegativeButton("LATER", null)
                        .setCancelable(false)
                        .show()

                return@OnSharedPreferenceChangeListener
            }
        }
    }

    private fun restartApp() {

        val intent = Intent(activity, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        activity.startActivity(intent)
        activity.finish()
    }

    override fun onResume() {
        super.onResume()
        PreferenceManager.getDefaultSharedPreferences(activity)
                .registerOnSharedPreferenceChangeListener(prefListener)
    }

    override fun onPause() {
        super.onPause()
        PreferenceManager.getDefaultSharedPreferences(activity)
                .unregisterOnSharedPreferenceChangeListener(prefListener)
    }
}