package com.dristov.rssapp.ui.history

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.dristov.rssapp.R
import com.dristov.rssapp.models.FeedHistoryItem
import com.dristov.rssapp.util.Utils

class HistoryItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var onClickListener: ((FeedHistoryItem) -> Unit)? = null

    val titleText: TextView
    val urlText: TextView

    init {

        titleText = itemView.findViewById(R.id.pageTitle) as TextView
        urlText = itemView.findViewById(R.id.pageDescription) as TextView
    }

    fun bindItem(item: FeedHistoryItem) {

        titleText.text = item.title
        urlText.text = Utils.getDomainName(item.url)

        itemView.setOnClickListener {
            onClickListener?.invoke(item)
        }
    }
}