package com.dristov.rssapp.ui.recyclerview;


import android.content.Context;
import android.content.res.Configuration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

import com.dristov.rssapp.R;


public class RecyclerViewEmpty extends RecyclerView {

    private View mEmptyView;

    private AdapterDataObserver mDataObserver = new AdapterDataObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            updateEmptyView();
        }
    };

    public RecyclerViewEmpty(Context context) {
        super(context);
    }

    public RecyclerViewEmpty(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RecyclerViewEmpty(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setEmptyView(View emptyView) {
        mEmptyView = emptyView;
    }

    @Override
    public void setAdapter(RecyclerView.Adapter adapter) {
        if (getAdapter() != null) {
            getAdapter().unregisterAdapterDataObserver(mDataObserver);
        }
        if (adapter != null) {
            adapter.registerAdapterDataObserver(mDataObserver);
        }
        super.setAdapter(adapter);
        updateEmptyView();
    }

    private void updateEmptyView() {
        if (mEmptyView != null && getAdapter() != null) {
            boolean showEmptyView = getAdapter().getItemCount() == 0;
            mEmptyView.setVisibility(showEmptyView ? VISIBLE : GONE);
        }
    }

    public void setLayoutSpan(Context ctx) {

        int orient = getResources().getConfiguration().orientation;
        int spanSize = 1;
        boolean isTablet = getResources().getBoolean(R.bool.isTablet);

        if (orient == Configuration.ORIENTATION_LANDSCAPE) {
            spanSize = (isTablet) ? 2 : 1;
        }

        if (spanSize == 1) {
            setLayoutManager(new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false));

        } else {
            setLayoutManager(new GridLayoutManager(ctx, spanSize, GridLayoutManager.VERTICAL, false));
        }
    }
}