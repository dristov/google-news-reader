package com.dristov.rssapp.ui.recyclerview;


import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

public class SimpleItemTouchHelperCallback extends ItemTouchHelper.Callback {

    private final ItemTouchHelperAdapter mAdapter;

    private boolean mDragEnabled;
    private boolean mSwipeEnabled;

    public SimpleItemTouchHelperCallback(ItemTouchHelperAdapter adapter)
    {
        mAdapter = adapter;
        mDragEnabled = true;
        mSwipeEnabled = true;
    }

    public SimpleItemTouchHelperCallback(ItemTouchHelperAdapter adapter, boolean dragEnabled, boolean swipeEnabled) {

        mAdapter = adapter;
        this.mDragEnabled = dragEnabled;
        this.mSwipeEnabled = swipeEnabled;
    }

    @Override
    public boolean isLongPressDragEnabled() {
        return mDragEnabled;
    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return mSwipeEnabled;
    }

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
        int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
        return makeMovementFlags(dragFlags, swipeFlags);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                          RecyclerView.ViewHolder target) {

        mAdapter.onItemMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        mAdapter.onItemDismiss(viewHolder.getAdapterPosition());
    }

}