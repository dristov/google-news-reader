package com.dristov.rssapp.ui.labels

import com.dristov.rssapp.models.Label
import com.dristov.rssapp.ui.base.MvpView

interface LabelsView : MvpView {

    fun showLabels(labels: List<Label>)

    fun onLabelAdded(label: Label)
    fun onLabelDeleted(pos: Int)
}