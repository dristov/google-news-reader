package com.dristov.rssapp.ui.history

import com.dristov.rssapp.models.FeedHistoryItem
import com.dristov.rssapp.ui.base.MvpView

interface HistoryView : MvpView {

    fun setItems(items: List<FeedHistoryItem>)
    fun onHistoryCleared()
}