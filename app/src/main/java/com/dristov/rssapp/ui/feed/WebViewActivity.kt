package com.dristov.rssapp.ui.feed

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.webkit.WebView
import android.webkit.WebViewClient

import com.dristov.rssapp.R
import com.dristov.rssapp.extensions.bind
import org.jsoup.Jsoup

class WebViewActivity : AppCompatActivity() {

    companion object {

        val EXTRA_WEB_URL = "url"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)

        val url = this.intent.extras.getString(EXTRA_WEB_URL, null)

        val webView = bind<WebView>(R.id.webView)
        webView.setWebViewClient(WebViewClient())

        if (url != null) {
            Log.v("WebViewActivity", "url = $url")
            webView.loadUrl(url)
        }
    }
}