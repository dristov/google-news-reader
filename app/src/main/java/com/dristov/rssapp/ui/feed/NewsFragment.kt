package com.dristov.rssapp.ui.feed

import android.os.Bundle
import android.support.v4.view.ViewPager
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dristov.rssapp.R
import com.dristov.rssapp.RssApp
import com.dristov.rssapp.extensions.inflate
import com.dristov.rssapp.extensions.isLollipop
import com.dristov.rssapp.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_news.*

class NewsFragment : BaseFragment(), ViewPager.OnPageChangeListener {

    override val toolbarTitle: String
        get() = "News"

    private var currentPosition = 0

    companion object {

        val BUNDLE_TOPIC_POS = "NewsFragment:topicPos"
        val KEY_TOPIC_POS = "NewsFragment:keyTopicPos"

        fun newInstance(pos: Int): NewsFragment {

            val args = Bundle()
            args.putInt(BUNDLE_TOPIC_POS, pos)

            val fragment = NewsFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        RssApp.getComponent(activity).inject(this)

        setupTransitions()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_news)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (savedInstanceState != null) {

            currentPosition = savedInstanceState.getInt(KEY_TOPIC_POS)

        } else if (this.arguments.containsKey(BUNDLE_TOPIC_POS)) {

            currentPosition = this.arguments.getInt(BUNDLE_TOPIC_POS)
        }

        setupPager(currentPosition)
    }

    override fun onResume() {
        super.onResume()
    }

    private fun setupPager(selected: Int) {

        val pagerAdapter = TopicPagerAdapter(childFragmentManager)
        viewPager.adapter = pagerAdapter
        viewPager.currentItem = selected
        viewPager.addOnPageChangeListener(this)

        tab_layout.setupWithViewPager(viewPager)
    }

    private fun setupTransitions() {

        if (isLollipop()) {
            this.enterTransition = TransitionInflater.from(activity).inflateTransition(R.transition.news_enter)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewPager.removeOnPageChangeListener(this)
    }

    override fun onPageScrollStateChanged(state: Int) { }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) { }

    override fun onPageSelected(position: Int) {
        currentPosition = position
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        outState?.putInt(KEY_TOPIC_POS, currentPosition)
    }
}