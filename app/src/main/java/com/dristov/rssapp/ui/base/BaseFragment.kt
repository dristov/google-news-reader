package com.dristov.rssapp.ui.base

import android.support.v4.app.Fragment
import com.dristov.rssapp.ui.MainActivity

public abstract class BaseFragment : Fragment() {

    protected abstract val toolbarTitle: String

    protected fun setToolbarTitle(title: String) {

        if (activity is MainActivity) {
            (activity as MainActivity).setToolbarTitle(title)
        }
    }

    override fun onStart() {
        super.onStart()
        setToolbarTitle(toolbarTitle)
    }
}