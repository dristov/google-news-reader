package com.dristov.rssapp.ui.feed

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.dristov.rssapp.ui.feed.FeedListFragment
import com.dristov.rssapp.util.TopicManager


class TopicPagerAdapter(val fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment? {
        val notation = TopicManager.topicList[position].shortName
        return FeedListFragment.newInstance(notation)
    }

    override fun getCount(): Int {
        return TopicManager.topicList.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return TopicManager.topicList[position].title
    }
}