package com.dristov.rssapp.ui.widget;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.dristov.rssapp.R;


public class SaveBookmarkDialog extends AppCompatDialogFragment implements DialogInterface.OnClickListener {

    public interface SaveBookmarkDialogListener {
        void saveBookmark(String inputText);
    }

    public static final String ARG_TITLE = "SaveDialog_argTitle";

    private EditText inputText;
    private SaveBookmarkDialogListener listener;

    public static SaveBookmarkDialog newInstance(String title) {

        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);

        SaveBookmarkDialog fragment = new SaveBookmarkDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public void setListener(SaveBookmarkDialogListener listener) {
        this.listener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        String title = getArguments().getString(ARG_TITLE);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.DefaultDialogStyle)
            .setTitle("Add to favorites")
            .setPositiveButton("SAVE", this)
            .setNegativeButton("CANCEL", null)
            .setCancelable(true);

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.view_prompt_save, null);

        inputText = (EditText) view.findViewById(R.id.promptInput);
        inputText.append(title);
        inputText.setSelection(inputText.getText().length());
        inputText.requestFocus();

        builder.setView(view);
        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

        if (listener != null) {
            listener.saveBookmark(inputText.getText().toString());
        }
    }
}