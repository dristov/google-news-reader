package com.dristov.rssapp.ui.history

import android.support.v7.widget.RecyclerView
import android.view.View
import com.dristov.rssapp.R
import com.dristov.rssapp.models.FeedHistoryItem
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection


class HistorySection(val title: String, val items: List<FeedHistoryItem>, val onClick: (FeedHistoryItem) -> Unit)
    : StatelessSection(R.layout.section_history, R.layout.item_page) {

    override fun getContentItemsTotal(): Int {
        return items.size
    }

    override fun onBindItemViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        (holder as HistoryItemViewHolder).bindItem(items[position])
        holder.onClickListener = onClick
    }

    override fun getItemViewHolder(view: View?): RecyclerView.ViewHolder? {
        return HistoryItemViewHolder(view!!)
    }

    override fun getHeaderViewHolder(view: View?): RecyclerView.ViewHolder? {
        return HistoryHeaderViewHolder(view!!)
    }

    override fun onBindHeaderViewHolder(holder: RecyclerView.ViewHolder?) {
        super.onBindHeaderViewHolder(holder)
        (holder as HistoryHeaderViewHolder).bind(title)
    }
}