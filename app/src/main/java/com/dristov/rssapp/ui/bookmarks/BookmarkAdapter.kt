package com.dristov.rssapp.ui.bookmarks

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dristov.rssapp.R
import com.dristov.rssapp.extensions.log_i
import com.dristov.rssapp.models.Bookmark
import com.dristov.rssapp.ui.recyclerview.ItemTouchHelperAdapter
import com.dristov.rssapp.util.Utils
import kotlinx.android.synthetic.main.item_bookmark.view.*

class BookmarkAdapter() : RecyclerView.Adapter<BookmarkAdapter.SimpleViewHolder>(),
        ItemTouchHelperAdapter {

    val bookmarkList = arrayListOf<Bookmark>()
    var bookmarksCopy = arrayListOf<Bookmark>()

    var onClickListener: ((Int) -> Unit)? = null
    var onMenuClick: ((Int) -> Unit)? = null
    var onDismissListener: ((Int) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): SimpleViewHolder? {
        val view = LayoutInflater.from(parent!!.context).inflate(R.layout.item_bookmark, parent, false)
        return SimpleViewHolder(view)
    }

    override fun onBindViewHolder(holder: SimpleViewHolder?, position: Int) {
        holder!!.bindItem(bookmarkList[position])
    }

    override fun getItemCount(): Int {
        return bookmarkList.size
    }

    fun setData(items: List<Bookmark>) {
        bookmarkList.addAll(items)
    }

    fun getItemAt(pos: Int): Bookmark {
        return bookmarkList[pos]
    }

    fun removeItemAt(pos: Int) {
        bookmarkList.removeAt(pos)
        notifyItemRemoved(pos)
    }

    override fun onItemMove(fromPosition: Int, toPosition: Int) {}

    override fun onItemDismiss(position: Int) {
        onDismissListener?.invoke(position)
        removeItemAt(position)
    }

    fun filter(query: String) {

        log_i("bookmark adapter filter")

        if (bookmarksCopy.isEmpty()) {
            bookmarksCopy.addAll(bookmarkList)
        }

        if (query.isEmpty()) {
            log_i("filter original list")
            bookmarkList.clear()
            bookmarkList.addAll(bookmarksCopy)

        } else {

            val result = bookmarksCopy.filter { it.title!!.toLowerCase().contains(query.toLowerCase())
                    || it.url!!.toLowerCase().contains(query.toLowerCase()) }

            log_i("result size = ${result.size}")

            bookmarkList.clear()
            bookmarkList.addAll(result)
        }

        notifyDataSetChanged()
    }

    inner class SimpleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        init {
            itemView.bkm_content.setOnClickListener(this)
            itemView.optionsMenu.setOnClickListener(this)
        }

        fun bindItem(bookmark: Bookmark) {

            with (bookmark) {

                itemView.pageTitle.text = title
                itemView.pageDescription.text = Utils.getDomainName(url)
            }
        }

        override fun onClick(v: View?) {

            when(v) {

                itemView.bkm_content -> onClickListener?.invoke(adapterPosition)
                itemView.optionsMenu -> onMenuClick?.invoke(adapterPosition)
            }
        }
    }
}