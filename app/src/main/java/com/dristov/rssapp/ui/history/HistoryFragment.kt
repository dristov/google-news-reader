package com.dristov.rssapp.ui.history

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import com.dristov.rssapp.R
import com.dristov.rssapp.RssApp
import com.dristov.rssapp.extensions.consume
import com.dristov.rssapp.extensions.openChromeCustomTab
import com.dristov.rssapp.models.FeedHistoryItem
import com.dristov.rssapp.ui.base.BaseListFragment
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class HistoryFragment : BaseListFragment(), HistoryView {

    override val toolbarTitle: String
        get() = "History"

    val sectionedAdapter by lazy {
        SectionedRecyclerViewAdapter()
    }

    @Inject
    lateinit var presenter: HistoryPresenter

    override val emptyViewText: String
        get() = "No items"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        RssApp.getComponent(activity).inject(this)
        presenter.attachView(this)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.loadItems()
    }

    override fun setItems(items: List<FeedHistoryItem>) {

        if (sectionedAdapter.itemCount > 0) {
            sectionedAdapter.removeAllSections()
            sectionedAdapter.notifyDataSetChanged()
        }

        val map = items.groupBy { getReadableDate(it.date!!) }

        val sections = arrayListOf<HistorySection>()

        for ((k,v) in map) {
            sections.add(HistorySection(k, v, {
                activity.openChromeCustomTab(it.url!!)
            }))
        }

        sections.forEach { sectionedAdapter.addSection(it) }
        recyclerView.adapter = sectionedAdapter
    }

    private fun getReadableDate(date: Date): String {

        val dateFormat = SimpleDateFormat("EEE, MMMM dd", Locale.getDefault())
        return dateFormat.format(date)
    }

    override fun onHistoryCleared() {
        sectionedAdapter.removeAllSections()
        sectionedAdapter.notifyDataSetChanged()
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {

        val searchItem = menu?.findItem(R.id.action_search)
        if (searchItem != null) {
            searchItem.isVisible = false
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_history, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?) = when(item?.itemId) {

        R.id.action_clear_history -> consume {

            if (sectionedAdapter.itemCount > 0) {
                presenter.clearHistory()
            }
        }

        else -> super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }
}