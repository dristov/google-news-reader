package com.dristov.rssapp.ui.feed

import com.dristov.rssapp.models.FeedItem
import com.dristov.rssapp.ui.base.MvpView

interface FeedView : MvpView {

    fun setItems(feedItems: List<FeedItem>)

    fun showLoadingBar()
    fun hideLoadingBar()

    fun openInBrowser(url: String)

    fun showSaveDialog(item: FeedItem)
    fun showShareDialog(url: String)

    fun showSnackbar(message: String)

    fun showErrorScreen();
}