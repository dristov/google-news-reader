package com.dristov.rssapp.ui.base;

import android.support.v4.app.Fragment;
import android.util.Log;

public abstract class BaseLazyFragment extends Fragment {

    protected boolean isVisible;
    private static final String TAG = "BaseLazyFragment";

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if(getUserVisibleHint()) {
            this.isVisible = true;
            onVisible();
        } else {
            this.isVisible = false;
        }
    }

    // when the fragment is visible to user
    protected void onVisible() {
        Log.i(TAG, "onVisible: ");
        loadContent();
    }

    protected abstract void loadContent();
}
