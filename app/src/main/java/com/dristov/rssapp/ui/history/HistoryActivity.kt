package com.dristov.rssapp.ui.history

import android.os.Bundle
import com.dristov.rssapp.R
import com.dristov.rssapp.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_history.*

class HistoryActivity : BaseActivity() {

    override val layoutResource: Int
        get() = R.layout.activity_history

    override fun onCreateCont(savedInstanceState: Bundle?) {

        setSupportActionBar(toolbarHistory)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        supportFragmentManager
                .beginTransaction()
                .add(R.id.content_frame, HistoryFragment())
                .commit()
    }
}