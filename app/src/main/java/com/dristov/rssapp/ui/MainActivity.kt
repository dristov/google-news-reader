package com.dristov.rssapp.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.Menu
import android.view.MenuItem
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem
import com.dristov.rssapp.R
import com.dristov.rssapp.RssApp
import com.dristov.rssapp.data.DbService
import com.dristov.rssapp.extensions.consume
import com.dristov.rssapp.extensions.transitionToActivity
import com.dristov.rssapp.ui.base.BaseActivity
import com.dristov.rssapp.ui.bookmarks.BookmarksFragment
import com.dristov.rssapp.ui.feed.NewsFragment
import com.dristov.rssapp.ui.history.HistoryActivity
import com.dristov.rssapp.ui.labels.LabelsFragment
import com.dristov.rssapp.ui.search.SearchActivity
import com.dristov.rssapp.ui.settings.SettingsActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity() {

    companion object {

        val TAG_FRAGMENT_NEWS = "News"
        val TAG_FRAGMENT_LABELS = "Labels"
        val TAG_FRAGMENT_BOOKMARKS = "Bookmarks"

        val KEY_CURRENT_FRAGMENT = "MainActivity:currentFragment"
    }

    override val layoutResource: Int
        get() = R.layout.activity_main

    private var currentFragmentTag = ""

    @Inject
    lateinit var dbService: DbService

    override fun onCreateCont(savedInstanceState: Bundle?) {

        RssApp.getComponent(this).inject(this)

        setSupportActionBar(toolbar)
        setupBottomBar()

        if (savedInstanceState == null) {

            showFragment(NewsFragment.newInstance(0), TAG_FRAGMENT_NEWS)

        } else {

            val frag = supportFragmentManager.findFragmentByTag(currentFragmentTag)

            if (frag != null) {
                showFragment(frag, currentFragmentTag)
            }
        }
    }

    fun setToolbarTitle(title: String) {
        supportActionBar?.title = title
    }

    private fun setupBottomBar() {

        val newsTab = AHBottomNavigationItem("News", R.drawable.newspaper)
        val favsTab = AHBottomNavigationItem("Favorites", R.drawable.heart)
        val labelsTab = AHBottomNavigationItem("Labels", R.drawable.label_outline)

        bottom_nav.addItem(newsTab)
        bottom_nav.addItem(favsTab)
        bottom_nav.addItem(labelsTab)

        bottom_nav.defaultBackgroundColor = ContextCompat.getColor(this, R.color.colorPrimary)
        bottom_nav.accentColor = ContextCompat.getColor(this, R.color.colorAccent)
        bottom_nav.inactiveColor = ContextCompat.getColor(this, R.color.white_50)
        bottom_nav.isBehaviorTranslationEnabled = false

        bottom_nav.setOnTabSelectedListener { pos, wasSelected ->

            when (pos) {

                0 -> consume {
                    showFragment(NewsFragment.newInstance(0), TAG_FRAGMENT_NEWS)
                }

                1 -> consume {
                    showFragment(BookmarksFragment(), TAG_FRAGMENT_BOOKMARKS)
                }

                2 -> consume {
                    showFragment(LabelsFragment(), TAG_FRAGMENT_LABELS)
                }

                else -> false
            }
        }
    }

    private fun showFragment(frag: Fragment, tag: String) {

        currentFragmentTag = tag

        supportFragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.content_frame, frag, tag)
                .commit()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putString(KEY_CURRENT_FRAGMENT, currentFragmentTag)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when(item.itemId) {

            R.id.action_search -> consume {
                transitionToActivity<SearchActivity>()
            }

            R.id.action_history -> consume {
                transitionToActivity<HistoryActivity>()
            }

            R.id.action_settings -> consume {
                transitionToActivity<SettingsActivity>()
            }

            else -> super.onOptionsItemSelected(item)
    }
}