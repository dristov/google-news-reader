package com.dristov.rssapp.ui.bookmarks

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.dristov.rssapp.R
import com.dristov.rssapp.RssApp
import com.dristov.rssapp.extensions.*
import com.dristov.rssapp.models.Bookmark
import com.dristov.rssapp.ui.base.BaseFragment
import com.dristov.rssapp.util.MenuSheetView
import com.dristov.rssapp.util.SimpleDividerItemDecoration
import kotlinx.android.synthetic.main.fragment_bookmarks.*
import javax.inject.Inject


class BookmarksFragment : BaseFragment(), BookmarksView, TextWatcher {

    override val toolbarTitle: String
        get() = "Favorites"

    private val bkmAdapter by lazy {
        BookmarkAdapter()
    }

    private val handler by lazy {
        Handler()
    }

    private lateinit var searchInput: EditText

    @Inject
    lateinit var presenter: BookmarksPresenter

    val filterTask = Runnable {
        bkmAdapter.filter(searchInput.text.toString())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        RssApp.getComponent(activity).inject(this)
        presenter.attachView(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = container?.inflate(R.layout.fragment_bookmarks)
        searchInput = view?.findViewById(R.id.search_input) as EditText
        searchInput.clearFocus()
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecyclerView()
        setupListeners()
        presenter.loadBookmarks()
    }

    override fun onResume() {
        super.onResume()
        setupSearch()
    }

    private fun setupRecyclerView() {
        recyclerView.setEmptyView(emptyView)
        recyclerView.setHasFixedSize(true)
        recyclerView.setLayoutSpan(activity)
        recyclerView.addItemDecoration(SimpleDividerItemDecoration(activity))
        recyclerView.addSwipeToDismiss(bkmAdapter)
    }

    private fun setupListeners() {

        bkmAdapter.onClickListener = {
            activity.openChromeCustomTab(bkmAdapter.getItemAt(it).url!!)
        }

        bkmAdapter.onMenuClick = {
            showBottomsheetMenu(it)
        }

        bkmAdapter.onDismissListener = {
            presenter.deleteBookmark(bkmAdapter.getItemAt(it).url!!)
        }
    }

    private fun setupSearch() {

        btn_clear.setOnClickListener {
            searchInput.setText("")
        }

        searchInput.addTextChangedListener(this)
    }

    private fun showBottomsheetMenu(pos: Int) {

        val item = bkmAdapter.getItemAt(pos)

        val menuSheetView = MenuSheetView(activity, item.title) {

            if (bottom_sheet.isSheetShowing) {
                bottom_sheet.dismissSheet()
            }

            when (it.itemId) {

                R.id.remove_bookmark -> {
                    presenter.deleteBookmark(item.url!!)
                    bkmAdapter.removeItemAt(pos)
                }

                R.id.share -> showShareDialog(item.url!!)
            }

            return@MenuSheetView true

        }

        menuSheetView.inflateMenu(R.menu.sheet_bookmark)
        bottom_sheet.showWithSheetView(menuSheetView)
    }

    override fun setBookmarks(bookmarks: List<Bookmark>) {

        if (bkmAdapter.itemCount > 0) {
            bkmAdapter.bookmarkList.clear()
        }

        bkmAdapter.setData(bookmarks)
        recyclerView.adapter = bkmAdapter

        if (bookmarks.size == 0) {
            search_container.hide()
        } else {
            search_container.show()
        }
    }

    fun showShareDialog(url: String) {

        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, url)
        activity.startActivity(Intent.createChooser(intent, "Share"))
    }

    override fun afterTextChanged(s: Editable?) {
        handler.removeCallbacks(filterTask)
        handler.postDelayed(filterTask, 700)

        if (s?.length == 0) {
            btn_clear.hide()
        } else {
            btn_clear.show()
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) { }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) { }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun onDestroyView() {
        search_input.removeTextChangedListener(this)
        super.onDestroyView()
    }
}