package com.dristov.rssapp.ui.base

import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.dristov.rssapp.R
import com.dristov.rssapp.RssApp
import com.dristov.rssapp.util.customtabs.CustomTabActivityHelper
import javax.inject.Inject

abstract class BaseActivity : AppCompatActivity() {

    @Inject
    lateinit var prefs: SharedPreferences

    val customTabActHelper by lazy { CustomTabActivityHelper() }

    protected var useChromeTabs = true

    override fun onCreate(savedInstanceState: Bundle?) {

        RssApp.getComponent(this).inject(this)

        if (prefs.getBoolean("theme_dark", true)) {
            setTheme(R.style.AppTheme_Dark)
        }
        super.onCreate(savedInstanceState)
        setContentView(layoutResource)

        onCreateCont(savedInstanceState)
    }

    abstract val layoutResource: Int
    abstract fun onCreateCont(savedInstanceState: Bundle?)

    override fun onStart() {
        super.onStart()
        if (useChromeTabs) {
            customTabActHelper.bindCustomTabsService(this)
        }
    }

    override fun onStop() {
        super.onStop()
        if (useChromeTabs) {
            customTabActHelper.unbindCustomTabsService(this)
        }
    }
}