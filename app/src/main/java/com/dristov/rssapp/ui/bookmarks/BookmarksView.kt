package com.dristov.rssapp.ui.bookmarks

import com.dristov.rssapp.models.Bookmark
import com.dristov.rssapp.ui.base.MvpView

interface BookmarksView : MvpView {

    fun setBookmarks(bookmarks: List<Bookmark>)
}