package com.dristov.rssapp.ui.settings

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.IntentCompat
import android.transition.Fade
import android.view.Menu
import android.view.MenuItem
import com.dristov.rssapp.R
import com.dristov.rssapp.extensions.consume
import com.dristov.rssapp.extensions.defaultSharedPreferences
import com.dristov.rssapp.extensions.isLollipop
import com.dristov.rssapp.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity : BaseActivity() {

    override val layoutResource: Int
        get() = R.layout.activity_settings

    override fun onCreateCont(savedInstanceState: Bundle?) {

        useChromeTabs = false

        setupTransition()

        setSupportActionBar(toolbarSettings)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        supportFragmentManager
                .beginTransaction()
                .add(R.id.settings_frame, SettingsFragment())
                .commit()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_settings, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?) = when(item!!.itemId) {

        R.id.action_reset_prefs -> consume { resetPreferences() }
        else -> super.onOptionsItemSelected(item)
    }

    private fun resetPreferences() {

        val editor = defaultSharedPreferences.edit()
        editor.clear()
        editor.commit()

        finish()
        val intent = intent
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(IntentCompat.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
    }

    private fun setupTransition() {

        if (isLollipop()) {

            val fade = Fade()
            fade.excludeTarget(android.R.id.statusBarBackground, true)
            fade.excludeTarget(android.R.id.navigationBarBackground, true)
            window.enterTransition = fade
        }
    }
}