package com.dristov.rssapp.ui.search

import com.dristov.rssapp.data.DbService
import com.dristov.rssapp.ui.base.BasePresenter
import javax.inject.Inject

class SearchPresenter @Inject constructor (val dbService: DbService) : BasePresenter<SearchNewsView>() {

    fun saveSearchQuery(query: String) {
        dbService.saveSearchItem(query)
    }

    fun loadRecentSearches() {
        mvpView.setRecentSearches(dbService.loadSearchItems())
    }

    fun clearSearches() {
        dbService.clearSearchHistory()
        mvpView.onSearchHistoryCleared()
    }
}