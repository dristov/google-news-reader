package com.dristov.rssapp.ui.labels

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dristov.rssapp.R
import com.dristov.rssapp.RssApp
import com.dristov.rssapp.extensions.inflate
import com.dristov.rssapp.extensions.isLollipop
import com.dristov.rssapp.extensions.transitionToActivity
import com.dristov.rssapp.models.Label
import com.dristov.rssapp.ui.base.BaseFragment
import com.dristov.rssapp.ui.search.SearchActivity
import com.dristov.rssapp.util.SimpleDividerItemDecoration
import kotlinx.android.synthetic.main.fragment_labels.*
import java.util.*
import javax.inject.Inject


class LabelsFragment : BaseFragment(), LabelsView {

    override val toolbarTitle: String
        get() = "Labels"

    @Inject
    lateinit var presenter: LabelsPresenter

    private lateinit var adapter: LabelsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        RssApp.getComponent(activity).inject(this)

        presenter.attachView(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_labels)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter.loadLabels()

        btn_add_label.setOnClickListener {

            if (!input_label.text.toString().trim().isBlank()) {
                presenter.addLabel(input_label.text.toString())
            }
        }
    }

    override fun showLabels(labels: List<Label>) {

        adapter = LabelsAdapter((ArrayList(labels)))

        adapter.onItemClick = { pos -> showFeedForLabel(adapter.labels[pos].text!!) }

        adapter.onItemDelete = { pos ->
            presenter.deleteLabel(labels[pos].id!!, pos)
        }

        labels_list.layoutManager = LinearLayoutManager(activity)
        labels_list.setHasFixedSize(true)
        labels_list.addItemDecoration(SimpleDividerItemDecoration(activity))
        labels_list.adapter = adapter
    }

    override fun onLabelAdded(label: Label) {
        adapter.insertItem(label)
        input_label.setText("")
    }

    override fun onLabelDeleted(pos: Int) {
        adapter.removeItem(pos)
    }

    private fun showFeedForLabel(label: String) {

        val intent = Intent(activity, SearchActivity::class.java)
        intent.putExtra(SearchActivity.EXTRA_SEARCH_QUERY, label)
        activity.transitionToActivity(intent)
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }
}