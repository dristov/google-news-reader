package com.dristov.rssapp.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.dristov.rssapp.R
import com.dristov.rssapp.extensions.bind
import com.dristov.rssapp.extensions.inflate
import com.dristov.rssapp.ui.recyclerview.RecyclerViewEmpty

abstract class BaseListFragment : BaseFragment() {

    protected lateinit var recyclerView: RecyclerViewEmpty
    protected abstract val emptyViewText: String

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = container?.inflate(R.layout.fragment_base_list)

        recyclerView = view!!.bind(R.id.recyclerView)

        val emptyView = view.bind<TextView>(R.id.emptyView)
        emptyView.text = emptyViewText
        setupRecyclerView(emptyView)

        return view
    }

    private fun setupRecyclerView(empty: View) {
        recyclerView.setEmptyView(empty)
        recyclerView.setHasFixedSize(true)
        recyclerView.setLayoutSpan(activity)
    }
}