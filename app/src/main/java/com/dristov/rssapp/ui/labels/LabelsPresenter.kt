package com.dristov.rssapp.ui.labels

import com.dristov.rssapp.data.DbService
import com.dristov.rssapp.ui.base.BasePresenter
import javax.inject.Inject

class LabelsPresenter @Inject constructor (val dbService: DbService) : BasePresenter<LabelsView>(){

    fun loadLabels() {
        val labels = dbService.loadLabels()
        mvpView.showLabels(labels)
    }

    fun addLabel(text: String) {
        val id = dbService.saveLabel(text)
        mvpView.onLabelAdded(dbService.findLabel(id))
    }

    fun deleteLabel(id: Long, pos: Int) {
        dbService.deleteLabel(id)
        mvpView.onLabelDeleted(pos)
    }
}