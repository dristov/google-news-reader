package com.dristov.rssapp.ui.history

import com.dristov.rssapp.data.DbService
import com.dristov.rssapp.ui.base.BasePresenter
import javax.inject.Inject


class HistoryPresenter @Inject constructor (val dbService: DbService) : BasePresenter<HistoryView>() {


    fun loadItems() {
        mvpView.setItems(dbService.loadHistoryItems())
    }

    fun clearHistory() {
        dbService.clearHistory()
        mvpView.onHistoryCleared()
    }
}