package com.dristov.rssapp.ui.search

import com.dristov.rssapp.models.SearchItem
import com.dristov.rssapp.ui.base.MvpView

interface SearchNewsView : MvpView {

    fun setRecentSearches(items: List<SearchItem>)
    fun onSearchHistoryCleared()
}