package com.dristov.rssapp.ui.labels

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.dristov.rssapp.R
import com.dristov.rssapp.extensions.inflate
import com.dristov.rssapp.models.Label
import kotlinx.android.synthetic.main.item_label.view.*
import java.util.*


class LabelsAdapter(val labels: ArrayList<Label>) : RecyclerView.Adapter<LabelsAdapter.LabelViewHolder>() {

    var onItemClick: ((Int) -> Unit)? = null
    var onItemDelete: ((Int) -> Unit)? = null

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): LabelViewHolder? {
        val view = parent?.inflate(R.layout.item_label)
        return LabelViewHolder(view)
    }

    override fun onBindViewHolder(holder: LabelViewHolder, position: Int) {
        holder.bindItem(labels[position])
    }

    override fun getItemCount(): Int {
        return labels.size
    }

    fun insertItem(label: Label) {
        labels.add(label)
        notifyItemInserted(labels.size-1)
    }

    fun removeItem(pos: Int) {
        labels.removeAt(pos)
        notifyItemRemoved(pos)
    }

    override fun getItemId(position: Int): Long {
        return labels[position].id!!
    }

    inner class LabelViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView),
            View.OnClickListener {

        init {

            itemView?.label_root?.setOnClickListener(this)
            itemView?.btn_delete?.setOnClickListener(this)
        }

        fun bindItem(lbl: Label) {
            itemView.label_text.text = lbl.text
        }

        override fun onClick(v: View?) {


            when (v) {

                itemView.label_root -> onItemClick?.invoke(adapterPosition)
                itemView.btn_delete -> onItemDelete?.invoke(adapterPosition)
            }
        }
    }
}