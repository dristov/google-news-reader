package com.dristov.rssapp.ui.history

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.section_history.view.*


class HistoryHeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(title: String) {

        itemView.section_date.text = title
    }
}