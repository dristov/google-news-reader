package com.dristov.rssapp.ui.feed

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.webkit.URLUtil
import com.dristov.rssapp.R
import com.dristov.rssapp.extensions.inflate
import com.dristov.rssapp.extensions.loadWithPicasso
import com.dristov.rssapp.models.FeedItem
import kotlinx.android.synthetic.main.item_feed.view.*
import java.util.*

class FeedAdapter(val rssItems: ArrayList<FeedItem>, val downloadImages: Boolean)
    : RecyclerView.Adapter<FeedAdapter.FeedViewHolder>() {

    var onItemClick: ((Int) -> Unit)? = null
    var onMenuClick: ((View, Int) -> Unit)? = null

    fun clearItems() {
        rssItems.clear()
        notifyDataSetChanged()
    }

    fun setItems(items: List<FeedItem>) {

        rssItems.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedViewHolder {
        val view = parent.inflate(R.layout.item_feed)
        return FeedViewHolder(view)
    }

    override fun onBindViewHolder(holder: FeedViewHolder, position: Int) {

        holder.bindItem(rssItems[position], downloadImages)
    }

    override fun getItemCount(): Int {
        return rssItems.size
    }

    inner class FeedViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        init {

            itemView.setOnClickListener(this)
            itemView.ctxMenuView.setOnClickListener(this)
        }

        fun bindItem(item: FeedItem, images: Boolean) {

            with(item) {

                itemView.feedTitle.text = title.substring(0, title.lastIndexOf("-") - 1)
                itemView.publisher.text = title.substring(title.lastIndexOf("-") + 2)
                itemView.pubDate.text = " - $relativeDate"

                if (images && URLUtil.isValidUrl(imageUrl)) {
                    itemView.feedImg.loadWithPicasso(imageUrl)

                } else {
                    itemView.feedImg.visibility = View.GONE
                    //moveLayoutToLeft(itemView)
                }
            }
        }

        override fun onClick(v: View) {

            when(v) {

                itemView -> onItemClick?.invoke(adapterPosition)
                itemView.ctxMenuView -> onMenuClick?.invoke(itemView.ctxMenu, adapterPosition)
            }
        }
    }
}