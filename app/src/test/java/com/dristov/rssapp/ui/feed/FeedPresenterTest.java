package com.dristov.rssapp.ui.feed;

import com.dristov.rssapp.data.DbService;
import com.dristov.rssapp.data.FeedRepository;
import com.dristov.rssapp.data.LoadFeedCallback;
import com.dristov.rssapp.models.FeedItem;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.matches;
import static org.mockito.Mockito.verify;

public class FeedPresenterTest {

    @Mock
    private FeedView view;

    @Mock
    private FeedRepository repo;

    @Mock
    private DbService dbService;

    @Captor
    private ArgumentCaptor<LoadFeedCallback> loadFeedCaptor;

    private FeedPresenter presenter;

    private List<FeedItem> feedItemList = new ArrayList<>();

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);

        addSomeFakeData();

        presenter = new FeedPresenter(repo, dbService);
        presenter.attachView(view);
    }

    @Test
    public void loadFeedFromRepoAndShowInView() {

        presenter.loadFeeds("tc", false);

        verify(view).showLoadingBar();

        // Callback is captured and invoked with fake feeds
        verify(repo).loadFeed(any(String.class), loadFeedCaptor.capture());
        loadFeedCaptor.getValue().onFeedLoaded(feedItemList);

        verify(view).hideLoadingBar();
        verify(view).setItems(feedItemList);
    }

    @Test
    public void clickOnArticle_openInBrowser() {

        String articleUrl = "http://example.com/article-1";

        presenter.openItemInBrowser(articleUrl);
        verify(view).openInBrowser(matches(articleUrl));
    }

    @After
    public void cleanUp() {
        presenter.detachView();
    }

    private void addSomeFakeData() {

        for (int i = 0; i < 3; i++) {

            FeedItem item1 = new FeedItem();
            item1.setTitle("Title" + i);
            item1.setDescription("Desc" + i);
            item1.setCategory("tc");
            item1.setLink("http://example.com/" + i);
            item1.setPubDate("");

            feedItemList.add(item1);
        }
    }
}